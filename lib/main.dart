import 'package:flutter/material.dart';
import 'package:lyncott/view/home.dart';
import 'package:lyncott/view/splash.dart';
import 'package:lyncott/controller/session_controller.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool _isLogged = true;
  Map<int, Color> colorSistema = {
    50: Colors.red,
    100: Colors.red,
    200: Colors.red,
    300: Colors.red,
    400: Colors.red,
    500: Colors.red,
    600: Colors.red,
    700: Colors.red,
    800: Colors.red,
    900: Colors.red,
  };

  _validateSession() async {
    SessionController _controller = SessionController();
    _isLogged = await _controller.isLogged();
    setState(() => _isLogged = _isLogged);
  }

  @override
  void initState() {
    super.initState();
    _validateSession();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Lyncott',
      theme: ThemeData(
        primarySwatch: MaterialColor(0xFFFF0000, colorSistema),
      ),
      home: _isLogged ? Home() : Splash(),
    );
  }
}
