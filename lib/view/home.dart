import 'package:flutter/material.dart';
import 'package:lyncott/controller/session_controller.dart';
import 'package:lyncott/model/user.dart';
import 'package:lyncott/view/acerca.dart';
import 'package:lyncott/view/convertidor.dart';
import 'package:lyncott/view/cronometro.dart';
import 'package:lyncott/view/recetario.dart';
import 'package:lyncott/view/recetas.dart';
import 'package:lyncott/view/singup.dart';

import 'favoritos.dart';

class Home extends StatefulWidget {
  static Route<dynamic> route() {
    return MaterialPageRoute(builder: (context) => Home());
  }

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  String selectedEntry = '';
  User _session;

  @override
  void initState() {
    super.initState();
    _getSession();
  }

  _getSession() async {
    SessionController sessionController = SessionController();
    _session = await sessionController.getSession();
    setState(() => _session = _session);
  }

  selectedTitle() {
    switch (selectedEntry) {
      case 'inicio':
        return 'Inicio';
      case 'favoritos':
        return 'Favoritos';
      case 'cronometro':
        return 'Cronómetro';
      case 'convertidor':
        return 'Convertidor de medidas';
      case 'recetario':
        return 'Recetario';
      case 'acerca':
        return 'Acerca de';
      default:
        return 'Lyncott';
    }
  }

  selectedFragment() {
    switch (selectedEntry) {
      case 'inicio':
        return Recetas();
      case 'favoritos':
        return Favoritos();
      case 'cronometro':
        return Cronometro();
      case 'convertidor':
        return Convertidor();
      case 'recetario':
        return Recetario();
      case 'acerca':
        return AcercaDe();
      default:
        return Recetas();
    }
  }

  selectedActions() {
    switch (selectedEntry) {
      case 'inicio':
        return <Widget>[
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () {},
          ),
          IconButton(
            icon: Icon(Icons.filter_list),
            onPressed: () {},
          )
        ];
      default:
        return null;
    }
  }

  selectedFloatingActions() {
    switch (selectedEntry) {
      case 'favoritos':
        return FloatingActionButton(
          onPressed: () {},
          child: Icon(Icons.add),
        );
      default:
        return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(selectedTitle()),
        actions: selectedActions(),
      ),
      body: selectedFragment(),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountName: Text(_session != null ? _session.username : ''),
              accountEmail: Text(_session != null ? _session.username : ''),
              currentAccountPicture: CircleAvatar(
                child: Text(
                  _session != null ? _session.username.substring(0)[0] : '',
                  style: TextStyle(fontSize: 40.0),
                ),
              ),
            ),
            ListTile(
              leading: Icon(Icons.home),
              title: Text('Inicio'),
              onTap: () {
                setState(() => selectedEntry = 'inicio');
                Navigator.of(context).pop();
              },
            ),
            ListTile(
              leading: Icon(Icons.star),
              title: Text('Favoritos'),
              onTap: () {
                setState(() => selectedEntry = 'favoritos');
                Navigator.of(context).pop();
              },
            ),
            ListTile(
              leading: Icon(Icons.timer),
              title: Text('Cronómetro'),
              onTap: () {
                setState(() => selectedEntry = 'cronometro');
                Navigator.of(context).pop();
              },
            ),
            ListTile(
              leading: Icon(Icons.compare_arrows),
              title: Text('Convertidor de medidas'),
              onTap: () {
                setState(() => selectedEntry = 'convertidor');
                Navigator.of(context).pop();
              },
            ),
            ListTile(
              leading: Icon(Icons.fastfood),
              title: Text('Recetario'),
              onTap: () {
                setState(() => selectedEntry = 'recetario');
                Navigator.of(context).pop();
              },
            ),
            ListTile(
              leading: Icon(Icons.info),
              title: Text('Acerca de'),
              onTap: () {
                setState(() => selectedEntry = 'acerca');
                Navigator.of(context).pop();
              },
            ),
            ListTile(
              leading: Icon(Icons.exit_to_app),
              title: Text('Cerrar sesión'),
              onTap: () {
                SessionController sessionController = SessionController();
                sessionController.closeSession();
                Navigator.of(context).pushReplacement(Singup.route());
              },
            ),
          ],
        ),
      ),
      floatingActionButton: selectedFloatingActions(),
    );
  }
}
