import 'package:flutter/material.dart';
import 'package:lyncott/controller/api.dart';
import 'package:lyncott/model/category.dart';
import 'package:lyncott/view/categoria_card.dart';

class Recetas extends StatefulWidget {
  @override
  _RecetasState createState() => _RecetasState();
}

class _RecetasState extends State<Recetas> {
  List<Category> categories = List();

  getCategories() async {
    API api = API();
    categories = await api.categories();
    setState(() => categories = categories);
  }

  @override
  void initState() {
    super.initState();
    getCategories();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: categories.length > 0
          ? ListView.builder(
              itemCount: categories.length,
              itemBuilder: (context, index) {
                return FlatButton(
                    onPressed: () {},
                    child: CategoriaCard(category: categories[index]));
              },
            )
          : CircularProgressIndicator(),
    );
  }
}
