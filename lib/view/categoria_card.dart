import 'package:flutter/material.dart';
import 'package:lyncott/model/category.dart';

class CategoriaCard extends StatelessWidget {
  final Category category;

  const CategoriaCard({this.category}) : super();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Card(
        elevation: 10,
        child: Stack(
          alignment: Alignment.topCenter,
          children: <Widget>[
            FittedBox(
              fit: BoxFit.fill,
              child: Image.network(
                  "https://vdmedia.elpais.com/elpaistop/20172/3/20170203131656_1486124879_still.jpg"),
            ),
            Text(
              category.name,
              style: TextStyle(color: Colors.white, fontSize: 20),
            )
          ],
        ),
      ),
    );
  }
}
