import 'package:flutter/material.dart';
import 'package:lyncott/controller/api.dart';
import 'package:lyncott/controller/session_controller.dart';
import 'package:lyncott/model/user.dart';
import 'package:lyncott/view/home.dart';

class Singup extends StatelessWidget {
  static Route<dynamic> route() {
    return MaterialPageRoute(builder: (context) => Singup());
  }

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  RegExp _userRegExp = RegExp(r'^([a-zA-Z0-9@.]+)$');
  String _user;
  String _password;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: NetworkImage("https://nubex.mx/app/images/splash.jpg"),
              fit: BoxFit.cover),
        ),
        child: Stack(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(vertical: 30),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'Cocinar es facil',
                    style: TextStyle(color: Colors.red, fontSize: 20),
                  ),
                  Text(
                    'cuándo tienes todo en tus manos',
                    style: TextStyle(color: Colors.red, fontSize: 20),
                  ),
                  Form(
                    key: formKey,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Column(
                        children: <Widget>[
                          TextFormField(
                            cursorColor: Colors.red,
                            style: TextStyle(color: Colors.white),
                            validator: (text) {
                              if (text.length == 0) {
                                return "El usuario es requerido";
                              } else if (!_userRegExp.hasMatch(text)) {
                                return "El formato no es correcto";
                              }
                              return null;
                            },
                            onSaved: (text) => _user = text,
                            keyboardType: TextInputType.emailAddress,
                            maxLength: 50,
                            decoration: InputDecoration(
                              labelText: 'Usuario',
                              labelStyle: TextStyle(color: Colors.red),
                              icon: Icon(Icons.mail, color: Colors.red),
                            ),
                          ),
                          TextFormField(
                            cursorColor: Colors.red,
                            style: TextStyle(color: Colors.white),
                            validator: (text) {
                              if (text.isEmpty) {
                                return "La contraseña es requerida";
                              }
                              return null;
                            },
                            onSaved: (text) => _password = text,
                            obscureText: true,
                            decoration: InputDecoration(
                              labelText: 'Contraseña',
                              labelStyle: TextStyle(color: Colors.red),
                              icon: Icon(Icons.lock, color: Colors.red),
                            ),
                          ),
                          Row(
                            children: <Widget>[
                              Expanded(
                                // Pendiente el cambio de color
                                // de texto y del botón
                                child: Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 30),
                                  child: RaisedButton(
                                    elevation: 10,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(50),
                                    ),
                                    textColor: Colors.white,
                                    color: Colors.red,
                                    child: Text('Iniciar sesión'),
                                    onPressed: () {
                                      if (formKey.currentState.validate()) {
                                        formKey.currentState.save();
                                        API api = API();
                                        Future<User> response = api.login({
                                          'username': _user,
                                          'password': _password
                                        });
                                        response.then((res) async {
                                          SessionController sessionController =
                                              SessionController();
                                          await sessionController
                                              .saveSession(res);
                                          Navigator.of(context)
                                              .pushReplacement(Home.route());
                                        });
                                      }
                                    },
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Text(
                        '¿No tienes cuenta aún?',
                        style: TextStyle(color: Colors.red, fontSize: 20),
                      ),
                      FlatButton(
                        child: Text(
                          'Regístrate',
                          style: TextStyle(
                              color: Colors.red,
                              fontWeight: FontWeight.bold,
                              fontSize: 20),
                        ),
                        onPressed: () {},
                      )
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
