import 'dart:async';

import 'package:flutter/material.dart';

class Cronometro extends StatefulWidget {
  @override
  _CronometroState createState() => _CronometroState();
}

class _CronometroState extends State<Cronometro> {
  String conteo = "00:00:00:00";
  final duracion = Duration(milliseconds: 1);
  var stopwatch = Stopwatch();

  void inicializar() {
    stopwatch.start();
    iniciar();
  }

  void continuar() {
    if (stopwatch.isRunning) {
      iniciar();
    }
    setState(() {
      var horas = stopwatch.elapsed.inHours.toString().padLeft(2, '0');
      var minutos =
          (stopwatch.elapsed.inMinutes % 60).toString().padLeft(2, '0');
      var segundos =
          (stopwatch.elapsed.inSeconds % 60).toString().padLeft(2, '0');
      var milisegundos =
          (stopwatch.elapsed.inMilliseconds % 60).toString().padLeft(2, '0');
      conteo = "$horas:$minutos:$segundos:$milisegundos";
    });
  }

  void iniciar() {
    Timer(duracion, continuar);
  }

  void reiniciar() {
    stopwatch.reset();
    setState(() => conteo = "00:00:00:00");
  }

  void pausar() {
    stopwatch.stop();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Expanded(
            flex: 6,
            child: Container(
              alignment: Alignment.center,
              child: Text(
                conteo,
                style: TextStyle(fontSize: 50, fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Expanded(
            flex: 4,
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  RaisedButton(
                    color: Colors.green,
                    elevation: 10,
                    onPressed: () => inicializar(),
                    child: Icon(Icons.play_arrow),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      RaisedButton(
                        color: Colors.blueGrey,
                        elevation: 10,
                        onPressed: () => reiniciar(),
                        child: Icon(Icons.refresh),
                      ),
                      RaisedButton(
                        color: Colors.red,
                        elevation: 10,
                        onPressed: () => pausar(),
                        child: Icon(Icons.pause),
                      ),
                    ],
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
