import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:lyncott/model/category.dart';
import 'package:lyncott/model/user.dart';
import 'package:lyncott/model/users_response.dart';

class API {
  String baseURL = 'https://crm.sparklabs.com.mx/lc/';

  Future<UsersResponse> users(Map<String, dynamic> body) async {
    var response = await http.post(baseURL + 'wp-json/wp/v2/users', body: body);

    return UsersResponse.fromJson(json.decode(response.body));
  }

  Future<User> login(Map<String, dynamic> body) async {
    var response =
        await http.post(baseURL + 'api/user/generate_auth_cookie', body: body);

    return User.fromJson(json.decode(response.body));
  }

  Future<List<Category>> categories() async {
    var respones = await http.get(baseURL + 'wp-json/wp/v2/categories');
    var list = json.decode(respones.body) as List;
    List<Category> categories = List();
    list.forEach((l) => categories.add(Category.fromJson(l)));
    return categories;
  }
}
