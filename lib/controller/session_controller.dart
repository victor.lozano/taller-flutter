import 'package:lyncott/model/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SessionController {
  isLogged() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getBool('isLogged') ?? false;
  }

  saveSession(User user) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('username', user.username);
    prefs.setBool('isLogged', true);
    prefs.setInt('id', user.id);
  }

  closeSession() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove('username');
    prefs.remove('id');
    prefs.setBool('isLogged', false);
  }

  getSession() async {
    final prefs = await SharedPreferences.getInstance();
    var username = prefs.getString('username');
    User user = User(username: username);
    return user;
  }
}
