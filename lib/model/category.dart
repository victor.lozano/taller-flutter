class Category {
  final int id;
  final String description;
  final String name;

  Category({this.id, this.description, this.name});

  factory Category.fromJson(Map<String, dynamic> json) {
    return Category(
        id: json['id'], description: json['description'], name: json['name']);
  }
}
