import 'data_response.dart';

class UsersResponse {
  String code;
  String message;
  DataResponse data;

  UsersResponse({this.code, this.message, this.data});

  factory UsersResponse.fromJson(Map<String, dynamic> json) {
    return UsersResponse(
      code: json['code'],
      message: json['message'],
      data: DataResponse.fromJson(json['data']),
    );
  }
}
