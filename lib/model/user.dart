class User {
  final int id;
  final String username;
  final String nicename;
  final String email;
  final String url;
  final DateTime registered;
  final String displayname;
  final String firstname;
  final String lastname;
  final String description;
  final String capabilities;
  final String avatar;

  User({
    this.id,
    this.username,
    this.nicename,
    this.email,
    this.url,
    this.registered,
    this.displayname,
    this.firstname,
    this.lastname,
    this.description,
    this.capabilities,
    this.avatar,
  });

  factory User.fromJson(Map<String, dynamic> json) {
    json = json['user'];
    return User(
        id: json['id'],
        username: json['username'],
        nicename: json['nicename'],
        email: json['email'],
        url: json['url'],
        registered: DateTime.parse(json['registered']),
        displayname: json['displayname'],
        firstname: json['firstname'],
        lastname: json['lastname'],
        description: json['description'],
        capabilities: json['capabilities'],
        avatar: json['avatar']);
  }
}
