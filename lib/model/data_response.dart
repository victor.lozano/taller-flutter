class DataResponse {
  int status;

  DataResponse({this.status});

  factory DataResponse.fromJson(Map<String, dynamic> json) {
    return DataResponse(status: json['status']);
  }
}
